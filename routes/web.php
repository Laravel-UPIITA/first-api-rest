<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Import clases
use App\Http\Middleware\ApiAuthMiddleware;

//Rutas del API

//UserController routes
  Route::post('/api/register', 'UserController@register');
  Route::post('/api/login', 'UserController@login');
  Route::put('/api/user/update', 'UserController@update');
  Route::post('/api/user/upload', 'UserController@upload')->middleware(ApiAuthMiddleware::class);
  Route::get('/api/user/avatar/{filename}', 'UserController@getImg');
  Route::get('/api/user/detail/{userId}', 'UserController@getUser');
//CategoryController routes
 Route::resource('/api/category', 'CategoryController');
//PostController router
  Route::resource('/api/post', 'PostController'); 
  Route::post('/api/post/upload', 'PostController@upload');
  Route::get('/api/post/image/{filename}', 'PostController@getImage');
  Route::get('/api/post/category/{id}', 'PostController@getPostsByCategory');
  Route::get('/api/post/user/{id}', 'PostController@getPostsByUser');