<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    //Onte to many relation
        public function posts(){
        return $this->hasMany('App\Post');
    }
}
