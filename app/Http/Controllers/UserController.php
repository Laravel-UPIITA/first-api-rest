<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;

class UserController extends Controller {
    public function register( Request $request ){
        //Get data from POST request
        $json = $request->input('json', null);
        $params = json_decode($json);
        $params_array =  json_decode($json, true);

        if(!empty($params) && !empty($params_array)){
          //Clean data
          $params_array = array_map('trim', $params_array);

          //Validate Data
          $validate = \Validator::make($params_array, [
            'name' => 'required|alpha',
            'surname' => 'required|alpha',
            'email' => 'required|email|unique:users',
            'password'  => 'required'
          ]);
          if ($validate->fails()){
            $data = array(
              'status' => 'error',
              'code'  => 404,
              'message'   => 'El usuario no se ha creado correctamente',
              'errors' => $validate->errors()
            );
            return response() -> json($data, 400);
          }else{
						//Password hash
						$pwd = hash('sha256', $params->password);
						//Create user
						$user = new User();
						$user->name =  $params_array['name'];
						$user->surname =  $params_array['surname'];
						$user->email =  $params_array['email'];
						$user->password =  $pwd;
						$user->role = 'ROLE_USER';
						//Save user
						$user->save();
            $data = array(
              'status' => 'success',
              'code'  => 200,
							'message'   => 'El usuario se ha creado correctamente',
							'user' => $user
            );
          }
        }else{
					$data = array(
						'status' => 'error',
						'code'  => 404,
						'message'   => 'Los datos enviiados no son correctos',
					);
				}
        return response()->json($data, $data['code']);
    }
    
    public function login( Request $request ){
        $jwtAuth = new \JwtAuth();
        //Get POST data
        $json = $request->input('json', null);
        $params = json_decode($json);
        $params_array = json_decode($json, true);
        //Validate data
        $validate = \Validator::make($params_array, [
          'email' => 'required|email',
          'password'  => 'required'
        ]);
        if ($validate->fails()){
          $signup = array(
            'status'  => 'error',
            'code' => 404,
            'message' => 'El usuario no se ha podido identificar',
            'errors' => $validate->errors()
          );
        }else{
          //Hash password
          $pwd = hash('sha256', $params->password);
          //Return Token or data
          $signup = $jwtAuth->signup($params->email, $pwd);
          if (!empty($params->gettoken)) {
            $signup = $jwtAuth->signup($params->email, $pwd,true);
          }
        }
        return response()->json($signup, 200);
    }

    public function update(Request $request){
      // Check if is an authorized user
      $token = $request->header('Authorization');
      $jwtAuth = new \JwtAuth();
      $checkToken = $jwtAuth->checkToken($token);
      //Get POST data
      $json = $request->input('json', null);
      $params_array = json_decode($json, true);      

      if ($checkToken && !empty($params_array)){
        //Get user ID
        $user = $jwtAuth->checkToken($token, true);
        //Validate data
        $validate = \Validator::make($params_array, [
          'name' => 'required|alpha',
          'surname' => 'required|alpha',
          'email' => 'required|email|unique:users,'.$user->sub      
        ]);
        //Remove data that won't be change
          unset($params_array['id']);
          unset($params_array['role']);
          unset($params_array['password']);
          unset($params_array['created_at']);
          unset($params_array['token_auth']);
        //Update DB
          $userUpdate = User::where('id', $user->sub)->update($params_array);
        //Return response
        $data = array(
          'code'  => 200,
          'status' => 'success',
          'user' => $user,
          'changes' => $params_array         
        );
      }else {
        $data = array(
          'code'  => 400,
          'status' => 'error',
          'message' => 'El usuario no está identificado'
        );
      }
      return response()->json($data, $data['code']);
    }

    public function upload(Request $request){
      //Get POST Data
      $img = $request->file('file0');
      //Validate img
      $validateImg = \Validator::make($request->all(), [
        'file0' => 'required|image:mimes:jpg,jpeg,png,gif'
      ]);

      //Upload img
      if (!$img || $validateImg->fails()){
        $data = array (
          'code'  => 400,
          'status' => 'error',
          'message' => 'Error al subir la imagen'        
        );     
      }else{
        $img_name= time().$img->getClientOriginalName();
        \Storage::disk('users')->put($img_name, \File::get($img));
        $data = array (
          'code'  => 200,
          'status' => 'success',
          'img' => $img_name      
        );   
      }
      return response()->json($data, $data['code']);
    }

    public function getImg($filename){
      $issetImg = \Storage::disk('users')->exists($filename);
      if ($issetImg) {
        $file = \Storage::disk('users')->get($filename);
        return new Response($file, 200);
      }else {
        $data = array (
          'code'  => 404,
          'status' => 'error',
          'message' => 'La imagen no existe'        
        );  
        return response()->json($data, $data['code']);
      }
    }

    public function getUser($userId){
      $user = User::find($userId);

      if (is_object($user)) {
        $data = array (
          'code'  => 200,
          'status' => 'success',
          'user' => $user
        );  
      }else {
        $data = array (
          'code'  => 404,
          'status' => 'error',
          'user' => 'El usuario no existe'
        );  
      }
      return response()->json($data, $data['code']);
    }
}
