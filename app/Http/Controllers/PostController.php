<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Post;
use App\Helpers\JwtAuth;

class PostController extends Controller {
	public function __construct(){
		$this->middleware('api.auth', ['except' =>[
																							'index', 
																							'show', 
																							'getImage',
																							'getPostsByCategory',
																							'getPostsByUser'
																							]]);
	}    

	public function	index(){
		$posts = Post::all()->load('category');

		return response()->json([
			'code' => 200,
			'status' => 'success',
			'posts' => $posts
		], 200);
	}

	public function show($id){
		$post = Post::find($id)->load('category');
		if (is_object($post)) {
			$data = [
				'code' => 200,
				'status' => 'success',
				'posts' => $post
			];
		}else {
			$data = [
				'code' => 404,
				'status' => 'error',
				'message' => 'El post no existe'
			];
		}
		return response()->json($data, $data['code']);
	}

	public function store(Request $request){
		//Get POST data
		$json = $request->input('json', NULL);
		$params = json_decode($json);
		$params_array = json_decode($json, true);

		if (!empty($params_array)) {
			//Get identify user
			$user = $this->getIdentity($request);
			//Validate Data
			$validate = \Validator::make($params_array, [
				'title' => 'required',
				'content' => 'required',
				'category_id' => 'required',
				'image' => 'required'
			]);

			if ($validate->fails()) {
				$data = [
					'code' => 400,
					'status'  => 'error',
					'message' => 'No se ha guardado el post, faltan datos'
				];
			}else {
				//Save post
				$post = new Post();
				$post->user_id = $user->sub;
				$post->category_id = $params->category_id;
				$post->title = $params->title;
				$post->content = $params->content;
				$post->image = $params->image;
				$post->save();

				$data = [
					'code' => 200,
					'status'  => 'success',
					'post' => $post
				];				
			}
		}else {
			$data = [
				'code' => 400,
				'status'  => 'error',
				'message' => 'No se han enviado los datos correctamente'
			];
		}
		//return
		return response()->json($data, $data['code']);
	}	

	public function update($id, Request $request){

		//Get post data
		$json = $request->input('json', NULL);
		$params_array = json_decode($json, true);

		//Default return data array 
		$data = array(
			'code' => 400,
			'status' => 'error',
			'message' => 'Datos enviados incorrectamente'
		);

		if (!empty($params_array)){
			//Validate data
			$validate = \Validator::make($params_array, [
				'title' => 'required',
				'content' => 'required',
				'category_id' => 'required'
			]);

			if ($validate->fails()){
				$data['errors'] = $validate->errors();
				return response()->json($data, $data['code']);
			}
			//Filtering data
			unset($params_array['id']);
			unset($params_array['user_id']);
			unset($params_array['created_at']);
			unset($params_array['user']);
			//get login user
			$user = $this->getIdentity($request);
			//Update entry

			//get post
			$post = Post::where('id', $id)
									->where('user_id', $user->sub)
									->first();
			/* $where = [
				'id' => $id,
				'user_id' => $user->sub
			]; 
			$post = Post::updateOrCreate($where, $params_array);*/
			if(!empty($post) && is_object($post)){

				//Update entry
				$post->update($params_array);
				//Return
				$data = array(
					'code' => 200,
					'status' => 'succes',
					'post' => $post,
					'changes' => $params_array
				);
			}

		}

		return response()->json($data, $data['code']);
	}

	public function destroy($id, Request $request){
		//get login user
		$user = $this->getIdentity($request);

		//get post
		$post = Post::where('id', $id)
								->where('user_id', $user->sub)
								->first();

		if (!empty($post)) {
			//Delete
			$post->delete();
			//return
			$data = [
				'code' => 200,
				'status' => 'success',
				'post' => $post
			];
		}else {
			$data = [
				'code' => 404,
				'status' => 'error',
				'message' => 'El post no existe'
			];
		}
		return response()->json($data, $data['code']);
	}	

	private function getIdentity($request){
		//get login user
		$jwtAuth = new JwtAuth();
		$token = $request->header('Authorization', NULL);
		$user = $jwtAuth->checkToken($token, true);

		return $user;
	}

	public function upload(Request $request){
		//Get img
		$image = $request->file('file0');
		//Validate img
		$validate = \Validator::make($request->all(),[
			'file0' => 'required|image|mimes:jpg,jpeg,png,gif'
		]);
		//Save img
		if (!$image || $validate->fails()) {
			$data = [
				'code' => 400,
				'status' => 'error',
				'message' => 'Error al subir la imagen'
			];
		}else{
			$image_name = time().$image->getClientOriginalName();
			\Storage::disk('posts')->put($image_name, \File::get($image));

			$data = [
				'code' => 200,
				'status' => 'success',
				'image' => $image_name
			];
		}
		//return
		return response()->json($data, $data['code']);

	}

	public function getImage($filename){
		//validate if img exist
		$isset = \Storage::disk('posts')->exists($filename);
		if ($isset) {
			//get img
			$file = \Storage::disk('posts')->get($filename);
			//Return img
			return new Response($file, 200);
		}else{
			//show errors
			$data = [
				'code' => 404,
				'status' => 'error',
				'message' => 'No pudimos encontrar la imagen'
			];
		}

		return response()->json($data, $data['code']);
	}

	public function getPostsByCategory($id){
		$posts = Post::where('category_id',  $id)->get();
		
		$data = [
			'code' => 200,
			'status' => 'success',
			'posts' => $posts
		];
		
		return response()->json($data, $data['code']);
	}

	public function getPostsByUser($id){
		$posts = Post::where('user_id',  $id)->get();
			
		$data = [
			'code' => 200,
			'status' => 'success',
			'posts' => $posts
		];

		return response()->json($data, $data['code']);
	}

}
