<?php
namespace App\Helpers;

use Firebase\JWT\JWT;
use Illumintate\Support\Facades\DB;
use App\User;

class JwtAuth{
  public $key;

  public function __construct() {
    $this->key = 'MFswDQYJKoZIhvcNAQEBBQADSgAwRwJAaiZiCRaO56xBrSPtbKmbi26V8SnP6J4a';
  }

  public function signup($email, $password, $getToken = null){
    //Find if user exist (email and password)
    $user = User::where([
      'email' => $email,
      'password' => $password
    ])->first();
    //Validate credentials
    $signup = false;
    if(is_object($user)){
      $signup = true;
    }
    //Generate token
    if($signup){
      $token = array(
        'sub' =>  $user->id,
        'email' => $user->email,
        'name'  => $user->name,
        'surname' => $user->surname,
        'iat' => time(),
        'exp' => time() + (7*24*3600)
      );
      $jwt = JWT::encode($token, $this->key, 'HS256');
      $decoded  = JWT::decode($jwt, $this->key, ['HS256']);
      //Return decode data or Token
      if (is_null($getToken)){
        $data = $jwt;
      }else{
        $data = $decoded;
      } 
    }else{
      $data = array(
        'status' => 'error',
        'message' => 'Login incorrecto'
      );
    }
    return $data;
  }

  public function checkToken($jwt, $getIdentidy = false){
    $auth = false;

    try{
      $jwt = str_replace('"', '', $jwt);
      $decoded = JWT::decode($jwt, $this->key, ['HS256']);
    }catch(\UnexpectedValudeException $e){
      $auth = false;
    }catch(\DomainException $e){
      $auth = false;
    }
    if (!empty($decoded) && is_object($decoded) && isset($decoded->sub)) {
      $auth = true;
    }else{
      $auth = false;
    }

    if ($getIdentidy){
      return $decoded;
    }

    return $auth;
  }
}